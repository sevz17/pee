CFLAGS ?= -Og -ggdb3 -Wall -Wextra -Werror

CFLAGS += -std=c11

all: pee

clean:
	rm -f pee *.o

.PHONY: all clean

pee.o: Makefile
